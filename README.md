

# vagrant2inventory

This scripts allows you to create an ansible inventory file
from a vagrant environment.

The idea is to spawn a number of machines using vagrant and configure
them using ansible as if you were configuring virtual machine.

# usage

cd to your vagrant folder then run:

```
vagrant ssh-config | vagrant2inventory
```

if it looks good and you want to make it persistent, then run:

```
vagrant ssh-config | vagrant2inventory | tee inventory
```

and you you can test that everything is working as it should:

```
esantoro@clifmi299:~/vagrants/kube-cluster01$ ansible -i inventory -m ping all
default | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
esantoro@clifmi299:~/vagrants/kube-cluster01$ 
```


# why?

You might wonder: what's wrong with regular vagrant-provided ansible
provisioner?

not much really, but  it falls short when you want to learn/practice
intermediate/advanced ansible concepts like roles because afaik the
anbile provisioner is designed to run a single playbook

# installation

you can either run the steps manually or use the included makefile.

## manual steps:

```
pip install -r requirements.txt
cp vagrant2inventory /usr/local/bin/
```

## using the makefile

```
sudo make install
```