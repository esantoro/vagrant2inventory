
.PHONY: install
install: requirements.txt vagrant2inventory
	pip3 install -r requirements.txt
	cp vagrant2inventory /usr/local/bin/
	chmod a+x /usr/local/bin/vagrant2inventory
